<?php
/* Smarty version 3.1.34-dev-7, created on 2020-03-17 09:45:42
  from '/home/hodi/domains/hodi.vn/public_html/content/themes/default/templates/games.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.34-dev-7',
  'unifunc' => 'content_5e709c46ad9b34_42185389',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '998e0676e4e9d7578ca23b2964ecc9abb68c48ea' => 
    array (
      0 => '/home/hodi/domains/hodi.vn/public_html/content/themes/default/templates/games.tpl',
      1 => 1581490866,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:_head.tpl' => 1,
    'file:_header.tpl' => 1,
    'file:_sidebar.tpl' => 2,
    'file:__feeds_game.tpl' => 1,
    'file:_footer.tpl' => 1,
  ),
),false)) {
function content_5e709c46ad9b34_42185389 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:_head.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
$_smarty_tpl->_subTemplateRender('file:_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<!-- page content -->
<div class="container mt20 offcanvas">
    <div class="row">

        <?php if ($_smarty_tpl->tpl_vars['view']->value == "game") {?>

            <!-- side panel -->
            <div class="col-12 d-block d-md-none offcanvas-sidebar">
                <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
            </div>
            <!-- side panel -->

            <!-- content panel -->
            <div class="col-12 offcanvas-mainbar">
                <div class="post">
                    <div class="ptb20 plr20">
                        <div class="post-header mb0">
                            <div class="post-avatar">
                                <div class="post-avatar-picture" style="background-image:url(<?php echo $_smarty_tpl->tpl_vars['game']->value['thumbnail'];?>
);">
                                </div>
                            </div>
                            <div class="post-meta">
                                <div class="float-right">
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/games" class="btn btn-sm btn-light d-none d-lg-block">
                                        <i class="fa fa-arrow-circle-left mr5"></i><?php echo __("Go Back");?>

                                    </a>
                                    <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/games" class="btn btn-sm btn-icon btn-light d-block d-lg-none">
                                        <i class="fa fa-arrow-circle-left"></i>
                                    </a>
                                </div>
                                <div class="h6 mt5 mb0"><?php echo $_smarty_tpl->tpl_vars['game']->value['title'];?>
</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe frameborder="0" src="<?php echo $_smarty_tpl->tpl_vars['game']->value['source'];?>
"></iframe>
                </div>
            </div>
            <!-- content panel -->

        <?php } else { ?>

            <!-- side panel -->
            <div class="col-md-4 col-lg-3 offcanvas-sidebar js_sticky-sidebar">
                <?php $_smarty_tpl->_subTemplateRender('file:_sidebar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
            </div>
            <!-- side panel -->

            <!-- content panel -->
            <div class="col-md-8 col-lg-9 offcanvas-mainbar">

                <!-- tabs -->
                <div class="content-tabs rounded-sm shadow-sm clearfix">
                    <ul>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == '') {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/games"><?php echo __("Discover");?>
</a>
                        </li>
                        <li <?php if ($_smarty_tpl->tpl_vars['view']->value == "played") {?>class="active"<?php }?>>
                            <a href="<?php echo $_smarty_tpl->tpl_vars['system']->value['system_url'];?>
/games/played"><?php echo __("Your Games");?>
</a>
                        </li>
                    </ul>
                </div>
                <!-- tabs -->

                <!-- content -->
                <div>
                    <?php if ($_smarty_tpl->tpl_vars['games']->value) {?>
                        <ul class="row">
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['games']->value, '_game');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['_game']->value) {
?>
                                <?php $_smarty_tpl->_subTemplateRender('file:__feeds_game.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('_tpl'=>'box'), 0, true);
?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>

                        <!-- see-more -->
                        <?php if (count($_smarty_tpl->tpl_vars['games']->value) >= $_smarty_tpl->tpl_vars['system']->value['max_results_even']) {?>
                            <div class="alert alert-post see-more js_see-more" data-get="<?php echo $_smarty_tpl->tpl_vars['get']->value;?>
">
                                <span><?php echo __("See More");?>
</span>
                                <div class="loader loader_small x-hidden"></div>
                            </div>
                        <?php }?>
                        <!-- see-more -->
                    <?php } else { ?>
                        <p class="text-center text-muted">
                            <?php echo __("No games to show");?>

                        </p>
                    <?php }?>
                </div>
                <!-- content -->

            </div>
            <!-- content panel -->

        <?php }?>

    </div>
</div>
<!-- page content -->

<?php $_smarty_tpl->_subTemplateRender('file:_footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
